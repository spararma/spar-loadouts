# SPAR Loadouts #

This repository contains up-to-date scripts related to the SPAR Loadouts Selection.

This is for mission creators so that they can make sure they have the most up-to-date loadout system at hand at all times.

### How to use it? ###

Download this repository and drop it into your `<mission>\Loadouts` folder.

### Useful scripts ###

#### Gear selection ####

To make any object an STF Gear Selector (via ACE), paste this in the init field:
`this execVM "Loadouts\NewSystem\Gear\STF\Gear.sqf";`

Similarly for SSO and SAF:
`this execVM "Loadouts\NewSystem\Gear\SSO\Gear.sqf";`
`this execVM "Loadouts\NewSystem\Gear\SAF\Gear.sqf";`


#### Armory ####

To make a weapon selector (via ACE), paste this in the init field:

`this execVM "Loadouts\NewSystem\Gear\STF\Armory.sqf";`

If you want SSO to also be able to select their gear from the same object, you can add this in the next line:
`this execVM "Loadouts\NewSystem\Gear\SSO\Armory.sqf";`

