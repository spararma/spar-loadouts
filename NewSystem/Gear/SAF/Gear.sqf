_heliPilotAction = ["SPAR_SAF_HeliPilot","Helicopter Pilot","",{ player execVM 'Loadouts\NewSystem\Gear\SAF\Presets\SAF_HELI_PILOT.sqf' },{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _heliPilotAction, true] call ace_interact_menu_fnc_addActionToObject;

_jetPilotAction = ["SPAR_SAF_JetPilot","Jet Pilot","",{ player execVM 'Loadouts\NewSystem\Gear\SAF\Presets\SAF_JET_PILOT.sqf' },{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _jetPilotAction, true] call ace_interact_menu_fnc_addActionToObject;


// _patches = ["SPAR_SAF_Patches","Patches","",{}, { true }] call ace_interact_menu_fnc_createAction;
// [_this, 0, ['ACE_MainActions'], _patches, true] call ace_interact_menu_fnc_addActionToObject;


// SAF_PATCHLIST = [
// 	['EAGLE', 'SPAR_EAG'],
// 	['FALCON', 'SPAR_FAL'],
// 	['VALKYRIE', 'SPAR_VAL'],
// 	['SERAPH', 'SPAR_SER']
// ];

// {
// 	_action = [format ['SPAR_SAF_Patches_%1', (_x select 1)],(_x select 0),"",compile (format ['[player, "%1"] call BIS_fnc_setUnitInsignia;', _x select 1]),{ true }] call ace_interact_menu_fnc_createAction;
// 	[_this, 0, ['ACE_MainActions', 'SPAR_SAF_Patches'], _action] call ace_interact_menu_fnc_addActionToObject;
// } forEach SAF_PATCHLIST;

// _patches = ["SPAR_SAF_Patches_Auto","Auto-detect","",{
// 	_groupName = str (group player);

// 	_foundIndex = SAF_PATCHLIST findIf { (_x select 0) in _groupName };
// 	if (_foundIndex != -1) then {
// 		_patch = SAF_PATCHLIST select _foundIndex;
// 		[player, (_patch select 1)] call BIS_fnc_setUnitInsignia;
// 	};
// }, { true }] call ace_interact_menu_fnc_createAction;
// [_this, 0, ['ACE_MainActions', 'SPAR_SAF_Patches'], _patches, true] call ace_interact_menu_fnc_addActionToObject;