
// "Exported from Arsenal by SabreShark";

// "[!] UNIT MUST BE LOCAL [!]";
if (!local _this) exitWith {};

// "Remove existing items";
removeAllWeapons _this;
removeAllItems _this;

_linkedItems = (assignedItems _this);
{
	if ((_x find "TFAR") != 0) then {
		_this unassignItem _x;
	};
};

removeUniform _this;
removeVest _this;
removeBackpack _this;
removeHeadgear _this;
removeGoggles _this;

// "Add weapons";
_this addWeapon "rhs_weap_mk18_bk";
_this addPrimaryWeaponItem "tier1_microt2_black";
_this addPrimaryWeaponItem "rhs_mag_30Rnd_556x45_Mk318_Stanag";
_this addWeapon "rhsusf_weap_glock17g4";
_this addHandgunItem "rhsusf_mag_17Rnd_9x19_FMJ";
_this addHandgunItem "hlc_acc_TLR1";

// "Add containers";
_this forceAddUniform "USP_SOFTSHELL_G3C_OR_BLK_RGR";
_this addVest "UK3CB_V_Pilot_Vest_MTP";

// "Add binoculars";
_this addWeapon "Binocular";

// "Add items to containers";
for "_i" from 1 to 5 do {_this addItemToUniform "ACE_elasticBandage";};
_this addItemToUniform "ACE_packingBandage";
for "_i" from 1 to 4 do {_this addItemToUniform "ACE_tourniquet";};
_this addItemToUniform "ACE_CableTie";
_this addItemToUniform "ACE_morphine";
_this addItemToUniform "ACE_EarPlugs";
_this addItemToUniform "ACE_MapTools";
_this addItemToUniform "SPAR_Beret_Blue";
_this addItemToVest "USP_PVS15";
_this addItemToVest "ACE_MicroDAGR";
_this addItemToVest "ACE_Chemlight_HiWhite";
_this addItemToVest "ACE_Chemlight_HiGreen";
_this addItemToVest "ACE_Chemlight_HiRed";
for "_i" from 1 to 3 do {_this addItemToVest "rhsusf_mag_17Rnd_9x19_FMJ";};
for "_i" from 1 to 4 do {_this addItemToVest "rhs_mag_30Rnd_556x45_Mk318_Stanag";};
_this addItemToBackpack "ACE_IR_Strobe_Item";
_this addItemToBackpack "ACE_Flashlight_MX991";
_this addItemToBackpack "ACE_HandFlare_Green";
_this addItemToBackpack "ACE_HandFlare_Green";
_this addItemToBackpack "rhs_mag_30Rnd_556x45_Mk318_Stanag";
for "_i" from 1 to 2 do {_this addItemToBackpack "ACE_splint";};
for "_i" from 1 to 5 do {_this addItemToBackpack "ACE_packingBandage";};
for "_i" from 1 to 5 do {_this addItemToBackpack "ACE_elasticBandage";};
for "_i" from 1 to 15 do {_this addItemToBackpack "ACE_quikclot";};
for "_i" from 1 to 4 do {_this addItemToBackpack "ACE_epinephrine";};

_this addHeadgear "rhsusf_hgu56p_visor";

// "Add items";
_this linkItem "ItemMap";
_this linkItem "ItemCompass";
_this linkItem "ItemWatch";
_this linkItem "ItemRadio";

player execVM 'TFAR_Freq.sqf';
