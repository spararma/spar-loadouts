comment "Exported from Arsenal by Vestarr";

comment "[!] UNIT MUST BE LOCAL [!]";
if (!local _this) exitWith {};

comment "Remove existing items";
removeAllWeapons _this;
removeAllItems _this;

_linkedItems = (assignedItems _this);
{
	if ((_x find "TFAR") != 0) then {
		_this unassignItem _x;
	};
};

removeUniform _this;
removeVest _this;
removeBackpack _this;
removeHeadgear _this;
removeGoggles _this;

comment "Add weapons";
_this addWeapon "rhsusf_weap_glock17g4";
_this addHandgunItem "hlc_acc_TLR1";
_this addHandgunItem "rhsusf_mag_17Rnd_9x19_JHP";

comment "Add containers";
_this forceAddUniform "SPAR_Uniform_PilotCoveralls";
_this addVest "SPAR_SRU21P_LPU9P_PCU15AP";

comment "Add binoculars";
_this addWeapon "rhsusf_bino_m24";

comment "Add items to containers";
for "_i" from 1 to 4 do {_this addItemToUniform "ACE_tourniquet";};
_this addItemToUniform "rhsusf_mag_17Rnd_9x19_JHP";
_this addItemToUniform "SPAR_Beret_Blue";
_this addItemToUniform "ACE_microDAGR";
_this addItemToUniform "ACE_Flashlight_MX991";
_this addItemToUniform "ACE_epinephrine";
for "_i" from 1 to 2 do {_this addItemToVest "rhsusf_mag_17Rnd_9x19_JHP";};
_this addItemToVest "SmokeShellGreen";
_this addItemToVest "SmokeShell";
_this addItem "USP_PVS15";
_this addItemToVest "ACE_morphine";
for "_i" from 1 to 5 do {_this addItemToVest "ACE_elasticBandage";};
for "_i" from 1 to 2 do {_this addItemToVest "ACE_HandFlare_Red";};
for "_i" from 1 to 2 do {_this addItemToVest "ACE_HandFlare_Green";};

// [_this] call SPAR_FlightGear_fnc_addJetPilotHelmet;
_this addHeadgear 'SPAR_JHMCS2_MBU20P';

comment "Add items";
_this linkItem "ItemMap";
_this linkItem "ItemAndroid";
_this linkItem "ItemCompass";
_this linkItem "ItemWatch";
_this linkItem "ItemRadio";

player execVM 'TFAR_Freq.sqf';
