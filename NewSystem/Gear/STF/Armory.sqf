// LIST OF WEAPONS:
STF_secondary_weapons_list = [
	['Glock 17', 'rhsusf_weap_glock17g4', ['rhsusf_mag_17Rnd_9x19_FMJ', 2]],
	['SIG SAUER P320 M17', 'Tier1_P320', ['Tier1_21Rnd_9x19_P320_FMJ', 2]],
	['M45A1','hgun_ACPC2_F', ['rhsusf_mag_7x45acp_MHP', 2]],
	['M320 GL', 'rhs_weap_M320', ['rhs_mag_M441_HE', 8]]
];

STF_primary_assault_weapons_list = [
	['M4A1/II ', 'rhs_weap_m4a1_blockII', [['ACE_30Rnd_556x45_Stanag_M995_AP_mag', 8]], ['rhsusf_acc_compm4']],	
	['M4A1/II + M203', 'rhs_weap_m4a1_blockII_M203_bk', [['ACE_30Rnd_556x45_Stanag_M995_AP_mag', 8], ['rhs_mag_M441_HE', 5]], ['rhsusf_acc_compm4']],
	['MK18', 'rhs_weap_mk18_bk', [['ACE_30Rnd_556x45_Stanag_M995_AP_mag', 8]], ['tier1_microt2_black']],
	['HK416', 'Tier1_HK416D145_CTR', [['ACE_30Rnd_556x45_Stanag_M995_AP_mag', 8]], ['rhsusf_acc_compm4']],
	['SCAR-H', 'rhs_weap_SCARH_FDE_STD', [['rhs_mag_20Rnd_SCAR_762x51_m80_ball', 8]], ['rhsusf_acc_acog_rmr', 'tier1_scar_la5_side']]
];

STF_primary_lmg_weapons_list = [
	['M249', 'rhs_weap_m249_pip_S_para', [['rhsusf_100Rnd_556x45_mixed_soft_pouch_coyote', 4]], ['rhsusf_acc_su230_mrds', 'tier1_saw_bipod']],
	['MK48 Mod 0 Para', 'Tier1_MK48_Mod0_Para', [['Tier1_100Rnd_762x51_Belt_M80', 4]], ['rhsusf_acc_su230_mrds', 'tier1_saw_bipod_2']],
	['FN Minimi SPW', 'LMG_03_F', [['rhsusf_100Rnd_556x45_M995_soft_pouch_ucp', 4]], ['rhsusf_acc_su230_mrds']]
];

STF_primary_marksman_weapons_list = [
	['Mk 12 SPR', 'SPAR_STF_Basic_Mk12_SPR', [['rhs_mag_20Rnd_556x45_Mk262_Stanag', 10]], []],
	['SR-25', 'Tier1_SR25_EC', [['rhsusf_20Rnd_762x51_SR25_m118_special_Mag', 10]], ['rhsusf_acc_m8541_low', 'tier1_harris_bipod_black']],
	['M14', 'rhs_weap_m14_rail_wd', [['rhsusf_20Rnd_762x51_m118_special_Mag', 10]], ['rhsusf_acc_m8541_low', 'rhsusf_acc_harris_swivel']],
	['M107', 'rhs_weap_M107', [['rhsusf_mag_10Rnd_STD_50BMG_M33', 8]], ['rhsusf_acc_m8541_low']]	
];

STF_primary_shotgun_weapons_list = [
	['Benelli MK4', 'prpl_benelli_pgs', [['prpl_8Rnd_12Gauge_Pellets', 10]], []]
];

STF_primary_smg_weapons_list = [
	['MP5A3', 'UK3CB_MP5A3', [['UK3CB_MP5_30Rnd_9x19_Magazine', 8]], []]
];

STF_launchers_weapons_list = [
	['M72A7', 'rhs_weap_m72a7', []],
	['M136', 'rhs_weap_M136', []],
	['MAAWS (HEAT)', 'launch_MRAWS_sand_F', ['MRAWS_HEAT_F', 3]],
	['MAAWS (HE)', 'launch_MRAWS_sand_F', ['MRAWS_HE_F', 3]],
	['FIM92 Stinger', 'rhs_weap_fim92', ['rhs_fim92_mag', 3]]
];

// LIST OF ATTACHMENTS
STF_primary_attachments_scopes_list = [
	[
		'RedDots',
		[
			['AimPoint CompM4', 'rhsusf_acc_compm4'],
			['Aimpoint Micro T2', 'tier1_microt2_black']
		]
	],
	[
		'Holographic',
		[
			['EoTech 553', 'tier1_eotech553_black'],
			['EoTech XPS3', 'tier1_exps3_0_black']
		]
	],
	[
		'Scopes',
		[
			['SU230', 'rhsusf_acc_su230'],
			['SU230 + MRDS', 'rhsusf_acc_su230_mrds'],
			['SU230A', 'rhsusf_acc_su230a'],
			['SU230A + MRDS', 'rhsusf_acc_su230a_mrds'],
			['ACOG + MRDS', 'rhsusf_acc_acog_rmr'],
			['Leupold M3 (black)', 'tier1_leupoldm3a_adm_black'],
			['Leupold M3 (sand)', 'tier1_leupoldm3a_adm_desert'],
			['M8541', 'rhsusf_acc_m8541']		
		]
	],
	[
		'LPVO',
		[
			['Vortex Razor II', 'tier1_razor_gen2_16']
		]
	]
];

STF_primary_attachments_grips_list = [
	[
		'MLOK Grips',
		[
			['Magpul RVG (black)', 'tier1_mvg_mlok_black'],
			['Magpul RVG (tan)', 'tier1_mvg_mlok_fde'],
			['Magpul AFG (black)', 'tier1_afg_mlok_black'],
			['Magpul AFG (tan)', 'tier1_afg_mlok_fde']
		]
	],
	[
		'RIS Grips',
		[
			['Magpul RVG (black)', 'rhsusf_acc_rvg_blk'],
			['Magpul TD Stubby (black)', 'rhsusf_acc_tdstubby_blk'],
			['Magpul TD Stubby (tan)', 'rhsusf_acc_tdstubby_tan'],
			['Magpul AFG (black)', 'rhsusf_acc_grip2'],
			['Magpul AFG (tan)', 'rhsusf_acc_grip2_tan']
		]
	],
	[
		'Bipods',
		[
			['Harris Bipod (black)', 'tier1_harris_bipod_black'],
			['Harris Bipod (tan)', 'tier1_harris_bipod_tan'],
			['SAW Bipod (sand)', 'tier1_saw_bipod_desert'],
			['SAW Bipod (tan)', 'tier1_saw_bipod'],
			['Grip-Pod (black)', 'tier1_grippod_black'],
			['Grip-Pod (tan)', 'tier1_grippod_tan']
		]
	]
];

STF_primary_attachments_lasers_list = [
	[
		'Surefire M300C',
		[
			['Black', 'tier1_m300c_black'],
			['Tan', 'tier1_m300c']
		]
	],
	[
		'AN/PEQ-15', [
			['Side', '"; [true] execVM "Loadouts\NewSystem\Gear\scripts\determinePEQVariant.sqf'],
			['Top', '"; [false] execVM "Loadouts\NewSystem\Gear\scripts\determinePEQVariant.sqf']
		]
	]
];

STF_Deployable_Weapons = [
	['M252 Mortar', 'removeBackpackGlobal player; player addBackpack "rhs_M252_Gun_Bag";'],
	['M252 Mortar Tripod', 'removeBackpackGlobal player; player addBackpack "rhs_M252_Bipod_Bag";']
];


// ACTIONS ADDING THINGS
_weaponsAction = ["SPAR_STF_Weapons","Weapons","",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _weaponsAction, true] call ace_interact_menu_fnc_addActionToObject;

_primaryWeaponsAction = ["SPAR_STF_Weapons_Primary","Primary","\rhssaf\addons\rhssaf_inventoryicons\data\weapons\rhs_weap_SCARH_STD_ca.paa",{},{ (vest player) != '' }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons'], _primaryWeaponsAction] call ace_interact_menu_fnc_addActionToObject;

_primaryWeaponsAssaultAction = ["SPAR_STF_Weapons_Primary_Assault","Assault Rifles","\Tier1_Weapons\HK416s\data\ui_HK416D10_CTR_ca.paa",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary'], _primaryWeaponsAssaultAction] call ace_interact_menu_fnc_addActionToObject;

{
	_name = _x select 0;
	_gunClass = _x select 1;
	_ammoInfo = _x select 2;
	_attachmentsInfo = _x select 3;
	_ammoItemsString = (_ammoInfo apply { format ['player addPrimaryWeaponItem "%1"; for "_i" from 1 to %2 do { player addItemToVest "%1" }; ', _x select 0, _x select 1] }) joinString '';
	_attachmentsString = (_attachmentsInfo apply { format ['player addPrimaryWeaponItem "%1"; ', _x] }) joinString '';
	_action = [format ['SPAR_STF_Weapons_Primary_%1', _gunClass],_name,"",compile (format ['if (primaryWeapon player != "") then { _magazines = primaryWeaponMagazine player; { player removeMagazines _x; } forEach _magazines; };  player addWeapon "%1"; %2 %3', _gunClass, _ammoItemsString, _attachmentsString]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Primary_Assault'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach STF_primary_assault_weapons_list;


_primaryWeaponsLMGAction = ["SPAR_STF_Weapons_Primary_LMG","LMGs","\Tier1_Weapons\SAW\data\ui_Mk48_Mod0_Desert_ca.paa",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary'], _primaryWeaponsLMGAction] call ace_interact_menu_fnc_addActionToObject;

{
	_name = _x select 0;
	_gunClass = _x select 1;
	_ammoInfo = _x select 2;
	_attachmentsInfo = _x select 3;
	_ammoItemsString = (_ammoInfo apply { format ['player addPrimaryWeaponItem "%1"; for "_i" from 1 to %2 do { player addItemToVest "%1" }; ', _x select 0, _x select 1] }) joinString '';
	_attachmentsString = (_attachmentsInfo apply { format ['player addPrimaryWeaponItem "%1"; ', _x] }) joinString '';
	_action = [format ['SPAR_STF_Weapons_Primary_%1', _gunClass],_name,"",compile (format ['if (primaryWeapon player != "") then { _magazines = primaryWeaponMagazine player; { player removeMagazines _x; } forEach _magazines; };  player addWeapon "%1"; %2 %3', _gunClass, _ammoItemsString, _attachmentsString]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Primary_LMG'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach STF_primary_lmg_weapons_list;

_primaryWeaponsMarksmanAction = ["SPAR_STF_Weapons_Primary_Marksman","Marksman and Sniper Rifles","\Tier1_Weapons\SR25\data\ui_sr25_ec_d_ca.paa",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary'], _primaryWeaponsMarksmanAction] call ace_interact_menu_fnc_addActionToObject;

{
	_name = _x select 0;
	_gunClass = _x select 1;
	_ammoInfo = _x select 2;
	_attachmentsInfo = _x select 3;
	_ammoItemsString = (_ammoInfo apply { format ['player addPrimaryWeaponItem "%1"; for "_i" from 1 to %2 do { player addItemToVest "%1" }; ', _x select 0, _x select 1] }) joinString '';
	_attachmentsString = (_attachmentsInfo apply { format ['player addPrimaryWeaponItem "%1"; ', _x] }) joinString '';
	_action = [format ['SPAR_STF_Weapons_Primary_%1', _gunClass],_name,"",compile (format ['if (primaryWeapon player != "") then { _magazines = primaryWeaponMagazine player; { player removeMagazines _x; } forEach _magazines; };   player addWeapon "%1"; %2 %3', _gunClass, _ammoItemsString, _attachmentsString]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Primary_Marksman'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach STF_primary_marksman_weapons_list;

_primaryWeaponsShotgunAction = ["SPAR_STF_Weapons_Primary_Shotgun","Shotguns","\prpl_benelliM4\data\inv\gear_benelli18_x_ca.paa",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary'], _primaryWeaponsShotgunAction] call ace_interact_menu_fnc_addActionToObject;

{
	_name = _x select 0;
	_gunClass = _x select 1;
	_ammoInfo = _x select 2;
	_attachmentsInfo = _x select 3;
	_ammoItemsString = (_ammoInfo apply { format ['player addPrimaryWeaponItem "%1"; for "_i" from 1 to %2 do { player addItemToVest "%1" }; ', _x select 0, _x select 1] }) joinString '';
	_attachmentsString = (_attachmentsInfo apply { format ['player addPrimaryWeaponItem "%1"; ', _x] }) joinString '';
	_action = [format ['SPAR_STF_Weapons_Primary_%1', _gunClass],_name,"",compile (format ['if (primaryWeapon player != "") then { _magazines = primaryWeaponMagazine player; { player removeMagazines _x; } forEach _magazines; };  player addWeapon "%1"; %2 %3', _gunClass, _ammoItemsString, _attachmentsString]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Primary_Shotgun'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach STF_primary_shotgun_weapons_list;

_primaryWeaponsSmgAction = ["SPAR_STF_Weapons_Primary_SMGs","SMGs","\UK3CB_Factions\addons\UK3CB_Factions_Weapons\MP5\data\ui\gear_mp5a3_ca.paa",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary'], _primaryWeaponsSmgAction] call ace_interact_menu_fnc_addActionToObject;

{
	_name = _x select 0;
	_gunClass = _x select 1;
	_ammoInfo = _x select 2;
	_attachmentsInfo = _x select 3;
	_ammoItemsString = (_ammoInfo apply { format ['player addPrimaryWeaponItem "%1"; for "_i" from 1 to %2 do { player addItemToVest "%1" }; ', _x select 0, _x select 1] }) joinString '';
	_attachmentsString = (_attachmentsInfo apply { format ['player addPrimaryWeaponItem "%1"; ', _x] }) joinString '';
	_action = [format ['SPAR_STF_Weapons_Primary_%1', _gunClass],_name,"",compile (format ['if (primaryWeapon player != "") then { _magazines = primaryWeaponMagazine player; { player removeMagazines _x; } forEach _magazines; };  player addWeapon "%1"; %2 %3', _gunClass, _ammoItemsString, _attachmentsString]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Primary_SMGs'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach STF_primary_smg_weapons_list;

_attachmentsAction = ["SPAR_STF_Weapons_Attachments","Attachments","",{ },{ primaryWeapon player != "" }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary'], _attachmentsAction, true] call ace_interact_menu_fnc_addActionToObject;

_sightsAction = ["SPAR_STF_Weapons_Attachments_Optics","Optics","\Tier1_Weapons3\Optics\data\ui_Razor_Gen3_110_ADM_ca.paa",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments'], _sightsAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	_opticsType = _x select 0;
	_variants = _x select 1;

	_action = [format ['SPAR_STF_Weapons_Attachments_Optics_%1', _opticsType],_opticsType,"",{},{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments', 'SPAR_STF_Weapons_Attachments_Optics'], _action] call ace_interact_menu_fnc_addActionToObject;

	{
		_action = [format ['SPAR_STF_Weapons_Attachments_Optics_%1', _x select 1],(_x select 0),"",compile (format ['player addPrimaryWeaponItem "%1";', _x select 1]),{ true }] call ace_interact_menu_fnc_createAction;
		[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons','SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments', 'SPAR_STF_Weapons_Attachments_Optics', format ['SPAR_STF_Weapons_Attachments_Optics_%1', _opticsType]], _action] call ace_interact_menu_fnc_addActionToObject;
	} forEach _variants;	
} forEach STF_primary_attachments_scopes_list;

_gripsAction = ["SPAR_STF_Weapons_Attachments_Underbarrel","Underbarrel","\Tier1_Weapons3\Accessories\data\ui_AFG_MLOK_FDE_ca.paa",{},{  }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments'], _gripsAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	_gripType = _x select 0;
	_variants = _x select 1;

	_action = [format ['SPAR_STF_Weapons_Attachments_Underbarrel_%1', _gripType],_gripType,"",{},{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments', 'SPAR_STF_Weapons_Attachments_Underbarrel'], _action] call ace_interact_menu_fnc_addActionToObject;

	{
		_action = [format ['SPAR_STF_Weapons_Attachments_Underbarrel_%1', _x select 1],(_x select 0),"",compile (format ['player addPrimaryWeaponItem "%1";', _x select 1]),{ true }] call ace_interact_menu_fnc_createAction;
		[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons','SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments', 'SPAR_STF_Weapons_Attachments_Underbarrel', format ['SPAR_STF_Weapons_Attachments_Underbarrel_%1', _gripType]], _action] call ace_interact_menu_fnc_addActionToObject;
	} forEach _variants;
} forEach STF_primary_attachments_grips_list;

_lasersAction = ["SPAR_STF_Weapons_Attachments_Lasers","Lasers/Flashlights","\Tier1_Weapons\Accessoires\data\ui_LA5_ca.paa",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments'], _lasersAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	_laserType = _x select 0;
	_variants = _x select 1;

	_action = [format ['SPAR_STF_Weapons_Attachments_Lasers_%1', _laserType],_laserType,"",{},{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments', 'SPAR_STF_Weapons_Attachments_Lasers'], _action] call ace_interact_menu_fnc_addActionToObject;

	{
		_action = [format ['SPAR_STF_Weapons_Attachments_Lasers_%1', _x select 1],(_x select 0),"",compile (format ['player addPrimaryWeaponItem "%1";', _x select 1]),{ true }] call ace_interact_menu_fnc_createAction;
		[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons','SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments', 'SPAR_STF_Weapons_Attachments_Lasers', format ['SPAR_STF_Weapons_Attachments_Lasers_%1', _laserType]], _action] call ace_interact_menu_fnc_addActionToObject;
	} forEach _variants;	
} forEach STF_primary_attachments_lasers_list;

_secondaryWeaponsAction = ["SPAR_STF_Weapons_Secondary","Secondary","\rhsusf\addons\rhsusf_inventoryicons\data\weapons\rhsusf_weap_glock17g4_ca.paa",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons'], _secondaryWeaponsAction] call ace_interact_menu_fnc_addActionToObject;

{
	_name = _x select 0;
	_gunClass = _x select 1;
	_ammoInfo = _x select 2;
	_ammoType = _ammoInfo select 0;
	_ammoCount = _ammoInfo select 1;
	_action = [format ['SPAR_STF_Weapons_Secondary_%1', _gunClass],_name,"",compile (format ['if (handgunWeapon player != "") then { _magazines = handgunMagazine player; { player removeMagazines _x; } forEach _magazines; };  player addWeapon "%1"; player addHandgunItem "%2"; for "_i" from 1 to %3 do { player addItem "%2" }', _gunClass, _ammoType, _ammoCount]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Secondary'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach STF_secondary_weapons_list;

_launchersWeaponsAction = ["SPAR_STF_Weapons_Launchers","Launchers","",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons'], _launchersWeaponsAction] call ace_interact_menu_fnc_addActionToObject;

{
	_name = _x select 0;
	_gunClass = _x select 1;
	_ammoInfo = _x select 2;
	_ammoItemsString = ""; 
	if ((count _ammoInfo) > 0) then {
		_ammoItemsString = format ['player addWeaponItem ["%1", "%2"]; for "_i" from 1 to %3 do { player addItemToBackpack "%2" }; ', _gunClass, _ammoInfo select 0, _ammoInfo select 1];
	};
	_action = [format ['SPAR_STF_Weapons_Launchers_%1', _gunClass],_name,"",compile (format ['player addWeapon "%1"; %2', _gunClass, _ammoItemsString]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Launchers'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach STF_launchers_weapons_list;

_deployableWeaponsAction = ["SPAR_STF_Weapons_Deployable","Deployable","",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons'], _deployableWeaponsAction] call ace_interact_menu_fnc_addActionToObject;

{
	_action = [format ['SPAR_STF_Weapons_Deployable_%1', (_x select 0)],(_x select 0),"",compile (_x select 1),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Deployable'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach STF_Deployable_Weapons;
