STF_NVG_Item = 'USP_PVS31_COMPACT';

STF_uniforms_list = [
	['Crye G3 Combat Uniform', 'USP_G3C_KP_MC']
];

STF_vests_list = [
	[
		'Crew Plate Carrier',
		[
			['Standard', 'SPAR_InfantryCarrier_STF_MC'],
			['Crewman (Lite)', 'SPAR_InfantryCarrier_Lite_STF_MC']
		]
	],
	[
		'Plate Carrier',
		[
			['Standard', 'SPAR_Platecarrier_STF_MC'],
			['Medic', 'SPAR_Platecarrier_Medic_STF_MC'],
			['Machinegunner', 'USP_CRYE_JPC_MG'],
			['Team Lead', 'USP_EAGLE_MBAV_LOAD_MC']
		]
	],
	[
		'SPCS',
		[
			['Standard', 'rhsusf_spcs_ocp_teamleader'],
			['Grenadier', 'rhsusf_spcs_ocp_grenadier'],
			['Medic', 'rhsusf_spcs_ocp_medic'],
			['Marksman', 'rhsusf_spcs_ocp_sniper']
		]
	],
	[
		'Non-Ballistic',
		[
			['Combat Belt', 'SPAR_CombatBelt_STF_MC'],
			['Webbed Tactical Vest', 'SPAR_WebbedVest_STF_MC'],
			['Bandolier', 'SPAR_Bandolier_STF_MC']
		]
	]
];

STF_headgear_list = [
	[
		'Non-Ballistic',
		[
			['Cap', 'player addHeadgear "SPAR_Ballcap_STF_MC";'],
			['Cap + Peltor', 'player addHeadgear "SPAR_Ballcap_Comtac_STF_MC";']
		]
	],
	[
		'Crewman Helmets',
		[
			['Helmet', 'player addHeadgear "H_HelmetCrew_I";']
		]
	],
	[
		'OPSCORE FAST',
		[
			['Helmet', 'player addHeadgear "USP_OPSCORE_FASTMTC_C";'],
			['+ Goggles', 'player addHeadgear "USP_OPSCORE_FASTMTC_C"; player addGoggles "SPAR_OAKLEY_SI2_MC_BLK";']
		]
	]
];

STF_backpacks_list = [
	['Medical FAST Backpack', 'SPAR_FAST_Rucksack_Medic_STF_MC'],
	['Toolbelt', 'SPAR_Toolbelt_STF_MC'],
	['FAST Backpack', 'SPAR_FAST_Rucksack_STF_MC'],
	['Large Infantry Backpack', 'SPAR_Infantry_Rucksack_STF_MC'],
	['Small CQB Backpack', 'SPAR_CQB_Backpack_STF_MC'],
	['Remove', 'SPAR_FAST_Rucksack_STF_MC"; removeBackpack player; "']
];

STF_presets_list = [
	['Recruit', 'RecruitLoadout.sqf'],
	['Rifleman', 'Default.sqf'],
	['Medic', 'Medic.sqf'],
	['Platoon Medic', 'Platoon_Medic.sqf'],
	['Crewman', 'Crewman.sqf'],
	['Crew Medic', 'Crew_Medic.sqf'],
	['Engineer', 'Engineer.sqf'],
	['Demolitions', 'Demolitions.sqf'],
	['Grenadier', 'Grenadier.sqf'],
	['Squad/Platoon Leader', 'Leader.sqf']
];

STF_PATCHLIST = [
	['Operator', 'SPAR_STF_OPER'],
	['Medic', 'SPAR_STF_MED'],
	['Candidate', 'SPAR_STF_CAN'],
	['Controller', 'SPAR_STF_CONT'],
	['Commander', 'SPAR_STF_COM'],
	['Remove', '']
];

_uniformsAction = ["SPAR_STF_Uniforms","Uniforms","",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _uniformsAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	_action = [format ['SPAR_STF_Uniforms_%1', (_x select 1)],(_x select 0),"",compile (format ['player forceAddUniform "%1"; player execVM "Loadouts\NewSystem\Gear\STF\UniformEquipment.sqf";', _x select 1]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Uniforms'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach STF_uniforms_list;


_vestsAction = ["SPAR_STF_Vests","Vests","",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _vestsAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	_vestType = _x select 0;
	_variants = _x select 1;

	_action = [format ['SPAR_STF_Vests_%1', _vestType],_vestType,"",{},{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Vests'], _action] call ace_interact_menu_fnc_addActionToObject;

	{
		_action = [format ['SPAR_STF_Vests_%1', _x select 1],(_x select 0),"",compile (format ['player addVest "%1"; player execVM "Loadouts\NewSystem\Gear\STF\VestEquipment.sqf";', _x select 1]),{ true }] call ace_interact_menu_fnc_createAction;
		[_this, 0, ['ACE_MainActions', 'SPAR_STF_Vests',format ['SPAR_STF_Vests_%1', _vestType]], _action] call ace_interact_menu_fnc_addActionToObject;
	} forEach _variants;
} forEach STF_vests_list;

_headgearAction = ["SPAR_STF_Headgear","Headgear","",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _headgearAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	_headgearType = _x select 0;
	_variants = _x select 1;

	_action = [format ['SPAR_STF_Headgear_%1', _headgearType],_headgearType,"",{},{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Headgear'], _action] call ace_interact_menu_fnc_addActionToObject;

	{
		_action = [format ['SPAR_STF_Headgear_%1', _x select 0],(_x select 0),"",compile (_x select 1),{ true }] call ace_interact_menu_fnc_createAction;
		[_this, 0, ['ACE_MainActions', 'SPAR_STF_Headgear',format ['SPAR_STF_Headgear_%1', _headgearType]], _action] call ace_interact_menu_fnc_addActionToObject;
	} forEach _variants;
} forEach STF_headgear_list;


_nodsAction = ["SPAR_Nods", "Night Vision Goggles", "", {}, {headgear player != ''}] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _nodsAction, true] call ace_interact_menu_fnc_addActionToObject;

_nodsActionTake = ["SPAR_Nods_Take", "Take", "", { player linkItem STF_NVG_Item }, {true}] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_Nods'], _nodsActionTake, true] call ace_interact_menu_fnc_addActionToObject;

_nodsActionRemove = ["SPAR_Nods_Take", "Leave", "", { player unlinkItem STF_NVG_Item }, {true}] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_Nods'], _nodsActionRemove, true] call ace_interact_menu_fnc_addActionToObject;

_backpacksAction = ["SPAR_STF_Backpacks","Backpacks","",{},{ true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _backpacksAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	_action = [format ['SPAR_STF_Backpacks_%1', (_x select 1)],(_x select 0),"",compile (format ['removeBackpack player; player addBackpack "%1"; { player removeItemFromBackpack _x } forEach (backpackItems player)', _x select 1]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Backpacks'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach STF_backpacks_list;

_presetsAction = ["SPAR_STF_Presets","Item presets","",{},{ (vest player) != "" }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _presetsAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	// todo;
	_action = [format ['SPAR_STF_Presets_%1', (_x select 1)],(_x select 0),"",compile (format ['execVM "Loadouts\NewSystem\Gear\STF\Presets\%1";', _x select 1]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Presets'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach STF_presets_list;


_patches = ["SPAR_STF_Patches","Patches","",{}, { true }] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _patches, true] call ace_interact_menu_fnc_addActionToObject;

{
	_action = [format ['SPAR_STF_Patches_%1', (_x select 1)],(_x select 0),"",compile (format ['[player, "%1"] call BIS_fnc_setUnitInsignia;', _x select 1]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Patches'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach STF_PATCHLIST;

// _patches = ["SPAR_STF_Patches_Auto","Auto-detect","",{
// 	_groupName = str (group player);

// 	_foundIndex = STF_PATCHLIST findIf { (_x select 0) in _groupName };
// 	if (_foundIndex != -1) then {
// 		_patch = STF_PATCHLIST select _foundIndex;
// 		[player, (_patch select 1)] call BIS_fnc_setUnitInsignia;
// 	};
// }, { true }] call ace_interact_menu_fnc_createAction;
// [_this, 0, ['ACE_MainActions', 'SPAR_STF_Patches'], _patches, true] call ace_interact_menu_fnc_addActionToObject;
