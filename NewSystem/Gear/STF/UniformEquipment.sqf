_currentItems = uniformItems _this;
{ _this removeItemFromUniform _x } forEach _currentItems;

for "_i" from 1 to 4 do {_this addItemToUniform "ACE_tourniquet";}; // to prevent passout before even reaching direct proximity medic in current statistics
for "_i" from 1 to 5 do {_this addItemToUniform "ACE_quikclot";}; 
for "_i" from 1 to 5 do {_this addItemToUniform "ACE_elasticBandage";}; 
_this addItemToUniform "ACE_packingBandage"; //Packing Bandage
//akin to US IFAK2 with adjustments for ARMA engine reality in SPAR mod scenario

for "_i" from 1 to 2 do {_this addItemToUniform "ACE_CableTie";}; 
_this addItemToUniform "ACE_EarPlugs";
_this addItemToUniform "ACE_Flashlight_MX991";
_this addItemToUniform "ACE_IR_Strobe_Item";
_this addItemToUniform "ACE_MapTools";

_this linkItem "ItemMap";
_this linkItem "ItemCompass";
_this linkItem "ItemWatch";
_this linkItem "ItemRadio";
_this linkItem "ItemGPS";

_this addWeapon "rhsusf_bino_m24_ARD";

if (!('SPAR_Beret_Green' in (items player))) then {
	_this addItemToUniform 'SPAR_Beret_Green';
};
