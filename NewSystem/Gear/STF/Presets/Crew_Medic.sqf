player addVest "SPAR_InfantryCarrier_OCP";
player addBackpack "SPAR_FAST_Rucksack_Medic_OCP";

player addItemToBackpack "ACE_EntrenchingTool";
player addItemToBackpack "ToolKit";
player addItemToBackpack "MineDetector";
player addItemToBackpack "SmokeShell";

player addItemToBackpack "ACE_surgicalKit";
for "_i" from 1 to 30 do {player addItemToBackpack "ACE_elasticBandage";};
for "_i" from 1 to 10 do {player addItemToBackpack "ACE_morphine";};
for "_i" from 1 to 10 do {player addItemToBackpack "ACE_epinephrine";};
for "_i" from 1 to 5 do {player addItemToBackpack "ACE_adenosine";};
for "_i" from 1 to 6 do {player addItemToBackpack "ACE_splint";};
for "_i" from 1 to 6 do {player addItemToBackpack "ACE_tourniquet";};
for "_i" from 1 to 10 do {player addItemToBackpack "ACE_salineIV_500";};
player addItemToBackpack "ACE_salineIV";