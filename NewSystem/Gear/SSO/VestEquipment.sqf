_currentItems = vestItems _this;
{ _this removeItemFromVest _x } forEach _currentItems;

_this addItemToVest "ACE_microDAGR";
_this addItemToVest "ACE_Flashlight_MX991";
_this addItemToVest "ACE_tourniquet";
_this addItemToVest "Knife_m3";

//this should not block vestslots but be addItemtoBackpack via preset
for "_i" from 1 to 2 do {_this addItemToVest "HandGrenade";};
for "_i" from 1 to 5 do {_this addItemToVest "ACE_quikclot";};
for "_i" from 1 to 2 do {_this addItemToVest "ACE_morphine";};
for "_i" from 1 to 2 do {_this addItemToVest "ACE_epinephrine";};
for "_i" from 1 to 2 do {_this addItemToVest "ACE_M84";};
for "_i" from 1 to 3 do {_this addItemToVest "ACE_CableTie";};

_this addItemToVest "ACE_Chemlight_HiRed";
_this addItemToVest "ACE_Chemlight_HiGreen";
_this addItemToVest "ACE_DefusalKit";
_this addItemToVest "ACE_IR_Strobe_Item";
_this addItemToVest "ItemcTabHCam";
