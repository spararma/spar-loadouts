SSO_GEAR_CONDITION = {
	(['PERSEUS', str group player, false] call BIS_fnc_inString) || player getVariable ["SPAR_Allow_SSO", false];
};

SSO_uniforms_list = [
	['Crye G3 Combat Uniform', 'USP_G3C_RS_CU_KP_MX_VQ_MC'],
	['Black softshell jacket + Crye G3 Trousers', 'USP_SOFTSHELL_G3C_KP_MX_BLK_MC']
];

SSO_vests_list = [
	[
		'Crye JPC/CPC',
		[
			['Light', 'USP_CRYE_JPC_FS'],
			['Assault', 'USP_CRYE_JPC_ASLT'],
			['Marksman', 'USP_CRYE_JPC_DMB'],
			['Team Lead', 'USP_CRYE_CPC_LEAD_BELT_MC']
		]
	]
];

SSO_headgear_list = [
	[
		'Caps',
		[
			['Cap', 'player addHeadgear "USP_BASEBALL_CAP_CT3";'],
			['Boonie', 'player addHeadgear "USP_BOONIE_HAT_MC";']
		]
	],
	[
		'OPSCORE FAST',
		[
			['Helmet', 'player addHeadgear "USP_OPSCORE_FASTMT_CMSW";'],
			['+ Balaclava', 'player addHeadgear "USP_OPSCORE_FASTMT_CMSW"; player addGoggles "SPAR_BALACLAVA2_CBR";']
		]
	]
];

SSO_Backpacks_List = [
	['Patrol Backpack', 'USP_PATROL_PACK_CB_CS_FH'],
	['Medical FAST Backpack', 'SPAR_FAST_Rucksack_Medic_STF_MC'],
	['Combat back panel', 'USP_PACK_HYDRATION'],
	['CQB Back panel', 'USP_PACK_POINTMAN'],
	['Camelbak', 'USP_HYDROPACK_MC'],
	['Long Range Radio (Small)', 'SPAR_tf_rt1523g_Ranger_Green'],
	['UAV (AR-2)', 'B_UAV_01_backpack_F'],
	['Gunbag', 'ace_gunbag_Tan'],
	['Remote Designator', 'B_Static_Designator_01_weapon_F'],
	['Remove', 'SPAR_FAST_Rucksack_STF_MC"; removeBackpack player; "']
];

SSO_Presets_List = [
	['Rifleman', 'Default.sqf'],
	['Medic', 'Medic.sqf'],
	['Breacher', 'Breacher.sqf'],
	['Engineer', 'Engineer.sqf'],
	['Demolitions', 'Demolitions.sqf'],
	['Marksman/Sniper', 'Marksman.sqf'],
	['JTAC', 'JTAC.sqf'],
	['Squad/Platoon Leader', 'Leader.sqf']
];

SSO_PATCHLIST = [
	['OPERATOR', 'SPAR_SSO_OPER'],
	['MEDIC', 'SPAR_SSO_MED'],
	['RECON', 'SPAR_SSO_REC'],
	['Remove', '']
];

_uniformsAction = ["SPAR_SSO_Uniforms","Uniforms","",{},SSO_GEAR_CONDITION] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _uniformsAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	_action = [format ['SPAR_SSO_Uniforms_%1', (_x select 1)],(_x select 0),"",compile (format ['player forceAddUniform "%1"; player execVM "Loadouts\NewSystem\Gear\SSO\UniformEquipment.sqf";', _x select 1]),SSO_GEAR_CONDITION] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_SSO_Uniforms'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_uniforms_list;


// ================== Vests ==================
_vestsAction = ["SPAR_SSO_Vests","Vests","",{},SSO_GEAR_CONDITION] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _vestsAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	_vestType = _x select 0;
	_variants = _x select 1;

	_action = [format ['SPAR_SSO_Vests_%1', _vestType],_vestType,"",{},{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_SSO_Vests'], _action] call ace_interact_menu_fnc_addActionToObject;

	{
		_action = [format ['SPAR_SSO_Vests_%1', _x select 1],(_x select 0),"",compile (format ['player addVest "%1"; player execVM "Loadouts\NewSystem\Gear\SSO\VestEquipment.sqf";', _x select 1]),SSO_GEAR_CONDITION] call ace_interact_menu_fnc_createAction;
		[_this, 0, ['ACE_MainActions', 'SPAR_SSO_Vests',format ['SPAR_SSO_Vests_%1', _vestType]], _action] call ace_interact_menu_fnc_addActionToObject;
	} forEach _variants;
} forEach SSO_vests_list;

_headgearAction = ["SPAR_SSO_Headgear","Headgear","",{},SSO_GEAR_CONDITION] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _headgearAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	_headgearType = _x select 0;
	_variants = _x select 1;

	_action = [format ['SPAR_SSO_Headgear_%1', _headgearType],_headgearType,"",{},{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_SSO_Headgear'], _action] call ace_interact_menu_fnc_addActionToObject;

	{
		_action = [format ['SPAR_SSO_Headgear_%1', _x select 0],(_x select 0),"",compile (_x select 1),{ true }] call ace_interact_menu_fnc_createAction;
		[_this, 0, ['ACE_MainActions', 'SPAR_SSO_Headgear',format ['SPAR_SSO_Headgear_%1', _headgearType]], _action] call ace_interact_menu_fnc_addActionToObject;
	} forEach _variants;
} forEach SSO_headgear_list;


_nodsAction = ["SPAR_Nods", "Night Vision Goggles", "", {}, SSO_GEAR_CONDITION] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _nodsAction, true] call ace_interact_menu_fnc_addActionToObject;

_nodsActionTake = ["SPAR_Nods_Take", "Take", "", { player linkItem "USP_GPNVG18" }, {true}] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_Nods'], _nodsActionTake, true] call ace_interact_menu_fnc_addActionToObject;

_nodsActionRemove = ["SPAR_Nods_Take", "Leave", "", { player unlinkItem "USP_GPNVG18" }, {true}] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_Nods'], _nodsActionRemove, true] call ace_interact_menu_fnc_addActionToObject;

_backpacksAction = ["SPAR_SSO_Backpacks","Backpacks","",{},SSO_GEAR_CONDITION] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _backpacksAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	_action = [format ['SPAR_SSO_Backpacks_%1', (_x select 1)],(_x select 0),"",compile (format ['removeBackpack player; player addBackpack "%1"; { player removeItemFromBackpack _x } forEach (backpackItems player)', _x select 1]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_SSO_Backpacks'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_Backpacks_List;

_presetsAction = ["SPAR_SSO_Presets","Item presets","",{},SSO_GEAR_CONDITION] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _presetsAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	// todo;
	_action = [format ['SPAR_SSO_Presets_%1', (_x select 1)],(_x select 0),"",compile (format ['execVM "Loadouts\NewSystem\Gear\SSO\Presets\%1";', _x select 1]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_SSO_Presets'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_Presets_List;

_patches = ["SPAR_SSO_Patches","Patches","",{}, SSO_GEAR_CONDITION] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions'], _patches, true] call ace_interact_menu_fnc_addActionToObject;

{
	_action = [format ['SPAR_SSO_Patches_%1', (_x select 1)],(_x select 0),"",compile (format ['[player, "%1"] call BIS_fnc_setUnitInsignia;', _x select 1]),{ true }] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_SSO_Patches'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_PATCHLIST;

// _patches = ["SPAR_SSO_Patches_Auto","Auto-detect","",{
// 	_groupName = str (group player);

// 	_foundIndex = SSO_PATCHLIST findIf { (_x select 0) in _groupName };
// 	if (_foundIndex != -1) then {
// 		_patch = SSO_PATCHLIST select _foundIndex;
// 		[player, (_patch select 1)] call BIS_fnc_setUnitInsignia;
// 	};
// }, { true }] call ace_interact_menu_fnc_createAction;
// [_this, 0, ['ACE_MainActions', 'SPAR_SSO_Patches'], _patches, true] call ace_interact_menu_fnc_addActionToObject;