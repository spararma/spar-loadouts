SSO_ARMORY_CONDITION = {
	(['PERSEUS', str group player, false] call BIS_fnc_inString) || player getVariable ["SPAR_Allow_SSO", false];
}; 

// LIST OF WEAPONS:
SSO_handguns_list = [
	['Glock 22', 'Tier1_Glock22_TB', ['Tier1_20Rnd_40SW_FMJ', 3]], // format: [label, classname, array of ammunition]
	['SIG SAUER P320 M17 TACTICAL', 'Tier1_P320_TB', ['Tier1_17Rnd_9x19_P320_JHP', 3]]
];

SSO_assault_rifle_list = [
	['AUG A3', 'uk3cb_auga3_blk', [['UK3CB_AUG_30Rnd_556x45_Magazine', 8]], ['tier1_rotexiiic_grey']], // format: [label, classname, array of ammunition types and count, array of default attachments]
	['ACR Remington CQB', 'SMA_ACRREMAFGCQB_N', [['rhs_mag_30Rnd_556x45_Mk318_PMAG_Tan', 8]], ['tier1_exps3_0_3xmag_desert_up', 'sma_sfpeq_acrtop_tan', 'sma_supp2smat_556']],
	['HK416 D10 MLOK', 'Tier1_HK416D10_MW9_CTR', [['rhs_mag_30Rnd_556x45_Mk318_PMAG_Tan', 8]], ['Tier1_MW_LA5_M600V_alt_Black']], 
	['SIG SAUER MCX .300 BLK', 'Tier1_SIG_MCX_115_Virtus_300BLK', [['Tier1_30Rnd_762x35_300BLK_SMK_PMAG', 8]], ['Tier1_MCX_LA5_M600V_Black']]
];

SSO_marksman_rifle_list = [
	['M110 ACS (6.5 Creedmoor)', 'Tier1_M110k5_ACS_65mm', [['Tier1_20Rnd_65x48_Creedmoor_SR25_Mag', 8]], ['tier1_urx4_ngal_top', 'tier1_harris_bipod_mlok_tan']],
	['MK14 EBR', 'rhs_weap_m14ebrri', [['rhsusf_20Rnd_762x51_m118_special_Mag', 8]], ['tier1_harris_bipod_black', 'tier1_ngal_top']],
	['XM2010', 'rhs_weap_XM2010', [['rhsusf_5Rnd_300winmag_xm2010', 8]], ['tier1_harris_bipod_black']],
	['XM2010 (Woodland)', 'rhs_weap_XM2010_sa', [['rhsusf_5Rnd_300winmag_xm2010', 8]], ['tier1_harris_bipod_black']],
	['XM2010 (Desert)', 'rhs_weap_XM2010_d', [['rhsusf_5Rnd_300winmag_xm2010', 8]], ['tier1_harris_bipod_black']]
];

SSO_lmg_list = [
	['MK 46 Mod 1', 'Tier1_MK46_Mod1_Savit_Desert', [['rhsusf_100Rnd_556x45_mixed_soft_pouch_coyote', 5]], ['tier1_elcan_156_c1_docter_fde_2d', 'tier1_saw_bipod', 'tier1_mk46mod1_la5']]
];

SSO_smg_list = [
	['MP5SD6', 'UK3CB_MP5SD6', [['UK3CB_MP5_30Rnd_9x19_Magazine', 8]], []]
];

// LIST OF ATTACHMENTS:
SSO_barrel_attachments_list = [
	['7.62/300 BLK Suppressor', 'tier1_srd762_black'],
	['Rotex III 5.56', 'tier1_rotexiiic_grey'],
	['M110 Suppressor', 'tier1_sandmans_desert'],
	['Mk 12 Suppressor', 'rhsusf_acc_nt4_black']
];

SSO_Holos_list = [
	['EoTech 553 + Magnifier', 'tier1_eotech553_3xmag_black_down'],
	['EoTech 553 + Magnifier (Tan)', 'tier1_eotech553_3xmag_tan_down'],
	['EoTech XPS3 + Magnifier', 'tier1_exps3_0_g33_black_down'],
	['EoTech XPS3 + Magnifier (Tan)', 'tier1_exps3_0_g33_tano_down']
];

SSO_redDotsList = [
	['Aimpoint Micro T2 + Magnifier', 'tier1_microt2_leap_g33_black_down'],
	['Aimpoint Micro T2 + Magnifier (Tan)', 'tier1_microt2_leap_g33_tan_down']
];

SSO_lpvo_list = [
	['Vortex Razor III', 'tier1_razor_gen3_110_adm'],
	['Vortex Razor III + Aimpoint T2', 'tier1_razor_gen3_110_adm_t2'],
	['Vortex Razor III + Docter III', 'tier1_razor_gen3_110_geissele_docter']
];

SSO_scopes_list = [
	['Leupold M3 + Docter III', 'tier1_leupoldm3a_geissele_docter_black'],
	['Leupold M3 + Docter III (Tan)', 'tier1_leupoldm3a_geissele_docter_tan'],
	['M8541 + Night Vision', 'rhsusf_acc_premier_anpvs27'],
	['Leupold MK4', 'rhsusf_acc_leupoldmk4_2'],
	['Leupold MK4 (Tan)', 'rhsusf_acc_leupoldmk4_2_d'],
	['Leupold MK4 (Tan) + Docter III', 'rhsusf_acc_leupoldmk4_2_mrds']
];

SSO_handgun_attachments_list = [
	['Streamlight TLR-1', 'tier1_tlr1'],
	['Pistol Suppressor (9mm/.40)', 'tier1_tirant9s']
];

{
	_name = _x select 0;
	_gunClass = _x select 1;
	_ammoInfo = _x select 2;
	_attachmentsInfo = _x select 3;
	_ammoItemsString = (_ammoInfo apply { format ['player addPrimaryWeaponItem "%1"; for "_i" from 1 to %2 do { player addItemToVest "%1" }; ', _x select 0, _x select 1] }) joinString '';
	_attachmentsString = (_attachmentsInfo apply { format ['player addPrimaryWeaponItem "%1"; ', _x] }) joinString '';
	_action = [format ['SPAR_STF_Weapons_Primary_%1', _gunClass],_name,"",compile (format ['if (primaryWeapon player != "") then { _magazines = primaryWeaponMagazine player; { player removeMagazines _x; } forEach _magazines; }; player addWeapon "%1"; %2 %3', _gunClass, _ammoItemsString, _attachmentsString]), SSO_ARMORY_CONDITION] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Primary_Assault'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_assault_rifle_list;

{
	_name = _x select 0;
	_gunClass = _x select 1;
	_ammoInfo = _x select 2;
	_attachmentsInfo = _x select 3;
	_ammoItemsString = (_ammoInfo apply { format ['player addPrimaryWeaponItem "%1"; for "_i" from 1 to %2 do { player addItemToVest "%1" }; ', _x select 0, _x select 1] }) joinString '';
	_attachmentsString = (_attachmentsInfo apply { format ['player addPrimaryWeaponItem "%1"; ', _x] }) joinString '';
	_action = [format ['SPAR_STF_Weapons_Primary_%1', _gunClass],_name,"",compile (format ['if (primaryWeapon player != "") then { _magazines = primaryWeaponMagazine player; { player removeMagazines _x; } forEach _magazines; }; player addWeapon "%1"; %2 %3', _gunClass, _ammoItemsString, _attachmentsString]), SSO_ARMORY_CONDITION] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Primary_Marksman'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_marksman_rifle_list;

{
	_name = _x select 0;
	_gunClass = _x select 1;
	_ammoInfo = _x select 2;
	_attachmentsInfo = _x select 3;
	_ammoItemsString = (_ammoInfo apply { format ['player addPrimaryWeaponItem "%1"; for "_i" from 1 to %2 do { player addItemToVest "%1" }; ', _x select 0, _x select 1] }) joinString '';
	_attachmentsString = (_attachmentsInfo apply { format ['player addPrimaryWeaponItem "%1"; ', _x] }) joinString '';
	_action = [format ['SPAR_STF_Weapons_Primary_%1', _gunClass],_name,"",compile (format ['if (primaryWeapon player != "") then { _magazines = primaryWeaponMagazine player; { player removeMagazines _x; } forEach _magazines; }; player addWeapon "%1"; %2 %3', _gunClass, _ammoItemsString, _attachmentsString]), SSO_ARMORY_CONDITION] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Primary_LMG'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_lmg_list;

{
	_name = _x select 0;
	_gunClass = _x select 1;
	_ammoInfo = _x select 2;
	_attachmentsInfo = _x select 3;
	_ammoItemsString = (_ammoInfo apply { format ['player addPrimaryWeaponItem "%1"; for "_i" from 1 to %2 do { player addItemToVest "%1" }; ', _x select 0, _x select 1] }) joinString '';
	_attachmentsString = (_attachmentsInfo apply { format ['player addPrimaryWeaponItem "%1"; ', _x] }) joinString '';
	_action = [format ['SPAR_STF_Weapons_Primary_%1', _gunClass],_name,"",compile (format ['if (primaryWeapon player != "") then { _magazines = primaryWeaponMagazine player; { player removeMagazines _x; } forEach _magazines; }; player addWeapon "%1"; %2 %3', _gunClass, _ammoItemsString, _attachmentsString]), SSO_ARMORY_CONDITION] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Primary_SMGs'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_smg_list;

{
	_name = _x select 0;
	_gunClass = _x select 1;
	_ammoInfo = _x select 2;
	_ammoType = _ammoInfo select 0;
	_ammoCount = _ammoInfo select 1;
	_action = [format ['SPAR_STF_Weapons_Secondary_%1', _gunClass],_name,"",compile (format ['if (handgunWeapon player != "") then { _magazines = handgunMagazine player; { player removeMagazines _x; } forEach _magazines; }; player addWeapon "%1"; player addHandgunItem "%2"; for "_i" from 1 to %3 do { player addItem "%2" }', _gunClass, _ammoType, _ammoCount]), SSO_ARMORY_CONDITION] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Secondary'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_handguns_list;

_underbarrelAction = ["SPAR_STF_Weapons_Attachments_Barrel","Barrel","\Tier1_Weapons\Accessoires\data\ui_Gemtech_Halo_DE_ca.paa",{}, SSO_ARMORY_CONDITION] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments'], _underbarrelAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	_action = [format ['SPAR_STF_Attachments_Primary_%1', (_x select 1)],(_x select 0),"",compile (format ['player addPrimaryWeaponItem "%1"', _x select 1]), SSO_ARMORY_CONDITION] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments', 'SPAR_STF_Weapons_Attachments_Barrel'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_barrel_attachments_list;

{
	_action = [format ['SPAR_STF_Attachments_Primary_Optics_%1', (_x select 1)],(_x select 0),"",compile (format ['player addPrimaryWeaponItem "%1"', _x select 1]), SSO_ARMORY_CONDITION] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons','SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments', 'SPAR_STF_Weapons_Attachments_Optics', 'SPAR_STF_Weapons_Attachments_Optics_Holographic'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_Holos_list;

{
	_action = [format ['SPAR_STF_Attachments_Primary_Optics_%1', (_x select 1)],(_x select 0),"",compile (format ['player addPrimaryWeaponItem "%1"', _x select 1]), SSO_ARMORY_CONDITION] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons','SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments', 'SPAR_STF_Weapons_Attachments_Optics', 'SPAR_STF_Weapons_Attachments_Optics_RedDots'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_redDotsList;

{
	_action = [format ['SPAR_STF_Attachments_Primary_Optics_%1', (_x select 1)],(_x select 0),"",compile (format ['player addPrimaryWeaponItem "%1"', _x select 1]), SSO_ARMORY_CONDITION] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons','SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments', 'SPAR_STF_Weapons_Attachments_Optics', 'SPAR_STF_Weapons_Attachments_Optics_LPVO'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_lpvo_list;

{
	_action = [format ['SPAR_STF_Attachments_Primary_Optics_%1', (_x select 1)],(_x select 0),"",compile (format ['player addPrimaryWeaponItem "%1"', _x select 1]), SSO_ARMORY_CONDITION] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons','SPAR_STF_Weapons_Primary', 'SPAR_STF_Weapons_Attachments', 'SPAR_STF_Weapons_Attachments_Optics', 'SPAR_STF_Weapons_Attachments_Optics_Scopes'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_scopes_list;


_handgunAttachmentsAction = ["SPAR_STF_Weapons_Attachments_Secondary","Attachments","",{ }, SSO_ARMORY_CONDITION] call ace_interact_menu_fnc_createAction;
[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Secondary'], _handgunAttachmentsAction, true] call ace_interact_menu_fnc_addActionToObject;

{
	_action = [format ['SPAR_STF_Weapons_Attachments_Secondary_%1', (_x select 1)],(_x select 0),"",compile (format ['player addHandgunItem "%1"', _x select 1]), SSO_ARMORY_CONDITION] call ace_interact_menu_fnc_createAction;
	[_this, 0, ['ACE_MainActions', 'SPAR_STF_Weapons', 'SPAR_STF_Weapons_Secondary', 'SPAR_STF_Weapons_Attachments_Secondary'], _action] call ace_interact_menu_fnc_addActionToObject;
} forEach SSO_handgun_attachments_list;