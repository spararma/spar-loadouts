_currentItems = uniformItems _this;
{ _this removeItemFromUniform _x } forEach _currentItems;

if (headgear _this != 'SPAR_Beret_Tan') then {
	_this addItemToUniform 'SPAR_Beret_Tan';
};

//IFAK2 representation
for "_i" from 1 to 4 do {_this addItemToUniform "ACE_tourniquet";}; 
for "_i" from 1 to 5 do {_this addItemToUniform "ACE_elasticBandage";};
for "_i" from 1 to 5 do {_this addItemToUniform "ACE_quikclot";}; 
_this addItemToUniform "ACE_packingBandage"; 

_this addWeapon "rhsusf_bino_m24_ARD";
_this addItemToBackpack "ACE_IR_Strobe_Item";
_this addItemToUniform "ACE_EarPlugs";
_this addItemToUniform "ACE_MapTools";

_this addWeapon "rhsusf_bino_m24_ARD";

_this linkItem "ItemMap";
_this linkItem "ItemCompass";
_this linkItem "ItemWatch";
_this linkItem "ItemAndroid";
_this linkItem "ItemRadio";
