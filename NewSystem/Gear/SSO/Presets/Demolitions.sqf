if (!("ACE_Clacker" in (items player))) then {
	player addItemToBackpack "ACE_Clacker";
};

if (!("ACE_wirecutter" in (items player))) then {
	player addItemToBackpack "ACE_wirecutter";
};

if (!("ACE_EntrenchingTool" in (items player))) then {
	player addItemToBackpack "ACE_EntrenchingTool";
};

player addItemToBackpack "ToolKit";
player addItemToBackpack "MineDetector";
for "_i" from 1 to 5 do {player addItemToBackpack "DemoCharge_Remote_Mag";};
for "_i" from 1 to 2 do {player addItemToBackpack "SmokeShell";};
