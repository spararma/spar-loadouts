params ['_side'];

switch(toLower (primaryWeapon player)) do {
	// STF Assault Weapons
	case 'rhs_weap_mk18_bk': { player addPrimaryWeaponItem (if (_side) then { 'tier1_mk18_la5_side' } else { 'tier1_mk18_la5_top' }) };
	case 'rhs_weap_scarh_fde_std': { player addPrimaryWeaponItem (if (_side) then { 'tier1_scar_la5_side' } else { 'tier1_scar_la5_top' }) };
	case 'tier1_hk416d145_ctr': { player addPrimaryWeaponItem (if (_side) then { 'tier1_416_la5_side' } else { 'tier1_416_la5_top' }) };
	case 'rhs_weap_m4a1_blockII': { player addPrimaryWeaponItem (if (_side) then { 'tier1_m4bii_la5_side' } else { 'tier1_m4bii_la5_top' }) };
	case 'rhs_weap_m4a1_blockII_M203_bk': { player addPrimaryWeaponItem 'tier1_m4bii_la5_side' };

	case 'hlc_rifle_auga3';
	case 'hlc_rifle_auga3_GL': { player addPrimaryWeaponItem 'tier1_la5_side' };

	// SSO Assault Weapons
	case 'tier1_hk416d10_mw9_ctr': { player addPrimaryWeaponItem (if (_side) then { 'tier1_mw_la5_side' } else { 'tier1_mw_la5_top' }) };
	case 'tier1_sig_mcx_115_virtus_300blk': { player addPrimaryWeaponItem (if (_side) then { 'tier1_mcx_la5_side' } else { 'tier1_mcx_la5_top' }) };
	case 'SMA_ACRREMAFGCQB_N': { player addPrimaryWeaponItem (if (_side) then { 'sma_anpeq15_tan' } else { 'sma_sfpeq_acrtop_tan' }) };

	// Marksman rifles
	case 'tier1_sr25_ec': { player addPrimaryWeaponItem (if (_side) then { 'tier1_sr25_la5_side' } else { 'tier1_sr25_la5_top' }) };
	case 'tier1_m110k5_acs_65mm': { player addPrimaryWeaponItem (if (_side) then { 'tier1_urx4_la5_side' } else { 'tier1_urx4_la5_top' }) };

	// LMGs
	case 'tier1_mk48_mod0_para';
	case 'tier1_mk46_mod1_savit_desert': { player addPrimaryWeaponItem 'tier1_mk46mod0_la5' };
	case 'LMG_03_F': { player addPrimaryWeaponItem 'tier1_la5_side' };

	// Disable Shotguns
	case 'prpl_benelli_pgs': {};

	default { player addPrimaryWeaponItem (if (_side) then { 'tier1_la5_side' } else { 'tier1_la5_top' }) };
};
